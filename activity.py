input_year = int(input("Please input a year:\n"))
if input_year > 0:
    if input_year%4 == 0:
        print(f"{input_year} is a leap year")
    else:
        print(f"{input_year} is not a leap year")
else:
    print("Please input a positive number")

row = int(input("Enter number of rows:\n"))
col = int(input("Enter number of columns:\n"))


for each_row in range(row):
    for each_col in range(col):
        print("*", end="")
    print("\n")
